# Backend (express.js and MySQL)

### Buat aplikasi REST API menggunakan framework expressjs dan mysql, dengan ketentuan sbg berikut:

1. Buat database mysql yang berisi tabel-table berikut, beserta relasi antar tabel yang dibutuhkan 

    - tabel mahasiswa

        |__mahasiswa__|
        |---|
        |nama|
        |NIM|
        |tanggal lahir|
        |tempat lahir|
        |tahun masuk|
        |kode program studi|

    - tabel program studi

        |__program studi__|
        |---|
        |kode|
        |nama|

    - tabel mata kuliah

        |__mata kuliah__|
        |---|
        |kode|
        |nama|
        |SKS|
        |kode program studi|

    - tabel KRS

        |__KRS__|
        |---|
        |NIM|
        |kode mata kuliah|

2. Buat REST API untuk:

    - Membuat, mengedit, mengupdate, dan menghapus data mahasiswa

    - Membuat, mengedit, mengupdate, dan menghapus data program studi

    - Membuat, mengedit, mengupdate, dan menghapus data mata kuliah

    - Membuat, mengedit, mengupdate, dan menghapus data krs

    - Mendata Jumlah SKS yang diambil tiap mahasiswa

    - List mata kuliah pada tiap program studi


3. Aplikasi dibuat dengan kriteria sebagai berikut:
    - Routing hanya berisi penanganan masukan user dan bentuk response ke user

    - Routing tidak boleh berisi logic atau peraturan penulisan dan pengambilan data dari database

    - Penamaan method/function harus sesuai dengan action yang dijalankan

    - Penamaan variabel dan parameter harus jelas, berorientasi pada objek yang dituju

    - Code Reusability diutamakan

    - Berikan comment yg menjelaskan aksi yg dijalankan oleh kode tersebut

    - Tiap table memiliki primary key yang jelas, bila diperlukan diperbolehkan untuk menambah kolom tambahan pada table sesuai dengan kebutuhan

    - sertakan juga dokumentasi API nya dalam bentuk file JSON hasil export dari Postman 
